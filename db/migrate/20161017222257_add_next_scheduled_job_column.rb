class AddNextScheduledJobColumn < ActiveRecord::Migration[5.0]
  def change
    change_table :campaigns do |t|
      t.string :next_job_id, default: nil
    end
  end
end
