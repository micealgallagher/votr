class CreateSuggestions < ActiveRecord::Migration[5.0]
  def change
    create_table :suggestions do |t|

    	t.string			:suggestion
    	t.integer			:votes, default: 0
      t.string      :approval_status,
    	t.boolean     :initial, default: false
      t.boolean     :current_suggestion, default: false
      t.boolean     :displayed_on_bigscreen, default: false
      t.boolean     :archived, default: false
      t.timestamp   :made_current_at
      t.references  :campaign, index: true, foreign_key: true

      t.timestamps
    end
  end
end
