class AddManuallyAdvanceColumn < ActiveRecord::Migration[5.0]
  def change
    change_table :campaigns do |t|
      t.boolean :manual_advance, default: false
    end
  end
end
