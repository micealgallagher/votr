class ModifyCampaignNames < ActiveRecord::Migration[5.0]
  def change
    change_table :campaigns do |t|
      t.rename :duration_for_current_suggestion, :current_suggestion_ttl
      t.rename :is_active, :active 
      t.rename :has_been_active, :was_active
    end
  end
end
