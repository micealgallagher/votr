class ModifySuggesitonColumnNames < ActiveRecord::Migration[5.0]
  def change
    change_table :suggestions do |t|
      t.rename :is_initial, :initial
      t.rename :is_current_suggestion, :current
    end
  end
end
