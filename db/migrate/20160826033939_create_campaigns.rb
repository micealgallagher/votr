class CreateCampaigns < ActiveRecord::Migration[5.0]
  def change
    create_table :campaigns do |t|

      t.string   :name
      t.integer  :pin
      t.string   :audio_cue
      t.string   :feature_image
      t.boolean  :allow_suggestions
      t.integer  :duration_between_suggestions
      t.integer  :current_suggestion_duration
      t.boolean  :approve_suggestions
      t.boolean  :active, default: false
      t.boolean  :was_active, default: false
      t.string   :closed_reason

      t.timestamps
    end
  end
end
