# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161018202521) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "campaigns", force: :cascade do |t|
    t.string   "name"
    t.integer  "pin"
    t.string   "audio_cue"
    t.string   "feature_image"
    t.boolean  "allow_suggestions"
    t.integer  "duration_between_suggestions"
    t.integer  "current_suggestion_ttl"
    t.boolean  "approve_suggestions"
    t.boolean  "active",                       default: false
    t.boolean  "was_active",                   default: false
    t.string   "closed_reason"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "next_job_id"
    t.boolean  "manual_advance",               default: false
  end

  create_table "nicknames", force: :cascade do |t|
    t.string "name"
  end

  create_table "suggestions", force: :cascade do |t|
    t.string   "suggestion"
    t.integer  "votes",                  default: 0
    t.string   "approval_status"
    t.boolean  "initial"
    t.boolean  "current",                default: false
    t.boolean  "displayed_on_bigscreen", default: false
    t.boolean  "archived",               default: false
    t.datetime "made_current_at"
    t.integer  "campaign_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["campaign_id"], name: "index_suggestions_on_campaign_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "suggestions", "campaigns"
end
