Rails.application.routes.draw do
  get 'site/index'


  resources :campaigns, only: [:new, :index, :create, :show, :update, :index] do
    get 'activation', on: :member
    get 'activate', on: :member
    get 'deactivate', on: :member

    resources :bigscreen, only: [:index]

    resources :suggestion, only: [:create, :new] do
      put 'approve', on: :member
      put 'reject', on: :member

      get "/approve" => 'suggestion#approve', as: 'approve'
      get "/reject" => 'suggestion#reject', as: 'reject'
    end

  get "/voting" => 'suggestion#new', as: 'voting'
  get "/moderate" => 'moderate#show', as: 'moderate'

  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'campaigns#index'
end
