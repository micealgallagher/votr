class VotingChangedJob < ApplicationJob
  queue_as :vote

  def perform(campaign_id)
    suggestions = Suggestion.where(:campaign_id => campaign_id).approved_not_archived.pluck(:id, :votes)

    ActionCable.server.broadcast "campaign:#{campaing_id}:voting", {
      command: 'VotingChanged',
      message: suggestions.as_json(:only => [:id, :votes], methods: :updated_at_as_integer)
    }
  end
end
