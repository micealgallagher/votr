class NewCurrentSuggestionJob < ApplicationJob
  queue_as :advance_suggestion

  def perform(campaign_id, suggestion_update)
    ActionCable.server.broadcast "campaign:#{campaign_id}:voting", {
        command: 'NewCurrentSuggestion',
        message: suggestion_update
      }
  end
end
