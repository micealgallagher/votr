class CloseCampaignJob < ApplicationJob
  queue_as :advance_suggestion

  def perform(campaign_id)
    campaign = Campaign.find(campaign_id)
    return unless campaign.present?

    logger.debug "MGDEV - CloseCampaignJob - campaign:#{campaign_id}:voting"
    ActionCable.server.broadcast "campaign:#{campaign_id}:voting", {
      command: 'CloseCampaign',
      message: ModerateController.render(
          partial: 'moderate/not_active',
          locals: { "@campaign": campaign }
        ).squish
    }
  end
end
