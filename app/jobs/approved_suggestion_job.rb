class ApprovedSuggestionJob < ApplicationJob
  queue_as :approval

  def perform(campaign_id, rank, suggestion_id)
    begin
      suggestion = Suggestion.find(suggestion_id)
      
      ActionCable.server.broadcast "campaign:#{campaign_id}:voting", {
        command: 'ApprovedSuggestion',
        id: suggestion_id,
        message: SuggestionController.render(
          partial: 'suggestion/single_suggestion',
          locals: { suggestion: suggestion, rank: rank }
        ).squish
      }
    rescue
      logger.debug "Suggestion #{suggestion_id} was not found for campaign #{campaign_id}"
    end
  end
end
