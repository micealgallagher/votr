class RejectedSuggestionJob < ApplicationJob
  queue_as :approval

  def perform(campaign_id, suggestion_id)
    ActionCable.server.broadcast "campaign:#{campaign_id}:voting", {
      command: 'RejectedSuggestion',
      message: suggestion_id
    }
  end
end
