class AdvanceSuggestionJob < ApplicationJob
  queue_as :advance_suggestion

  def perform(campaign_id)
    begin
      campaign = Campaign.find(campaign_id) 
      return unless campaign.present?

      current_suggestion = campaign.suggestions.current
      if current_suggestion.any?
        current_suggestion = current_suggestion.first
        current_suggestion.archive
        current_suggestion.save
      end

      most_voted_suggestion = campaign.suggestions.most_voted

      logger.debug "MGDEV - We have #{most_voted_suggestion.any?}"

      if most_voted_suggestion.any?
        most_voted_suggestion = most_voted_suggestion.first
        most_voted_suggestion.make_current
        most_voted_suggestion.save
        
        current_suggestion = most_voted_suggestion
        most_voted_suggestion = campaign.suggestions.most_voted

        suggestion_update = {
          :current_suggestion_id => current_suggestion.id,
          :current_suggestion => current_suggestion.suggestion,
          :most_voted_suggestion => if most_voted_suggestion.any? then most_voted_suggestion.first.suggestion else nil end,
          :remaining_duration => campaign.current_suggestion_ttl_as_seconds.to_i
        }

        logger.debug "MGDEV - AdvanceSuggestionJob Broadcast - campaign:#{campaign.id}:voting, parameters #{suggestion_update}"
        ActionCable.server.broadcast "campaign:#{campaign_id}:voting", {
          command: 'NewCurrentSuggestion',
          message: suggestion_update
        }

        job = AdvanceSuggestionJob.set(wait: (campaign.current_suggestion_ttl_as_seconds)).perform_later(campaign_id)
        campaign.next_job_id = job.provider_job_id
        campaign.save(:validate => false)
      else
        logger.debug "MGDEV - JOB - Closing campaign #{campaign_id} because there are no more suggestions"
        logger.debug "Job: #{job}"

        job = CloseCampaignJob.perform_later(@campaign_id)

        campaign.next_job_id = job.provider_job_id
        campaign.close :NO_REMAINING_SUGGESTIONS
        campaign.save(:validate => false)

        CloseCampaignJob.perform_later(campaign.id)
      end

    rescue => e
      logger.debug "Campaign #{campaign_id} was not found #{e}"
    end

  end
end
