$.fn.extend

  promoteSuggestions: (newSuggestionList) ->
    console.log "MGDEV - promteSuggestions"

    newList = @convertToArray(newSuggestionList)
    oldList = @getOldSuggestionList()

    console.log "MGDEV - newList"
    console.log "MGDEV - oldList"

    console.log newList
    console.log oldList

    @determineListMutations(newList, oldList)

    console.log "MGDEV - All should be equal"

    console.log newList
    console.log oldList    

  determineListMutations: (newList, oldList) ->
    pos = newList.length 

    # Determine what mutations are required to make lists have same order
    while --pos > -1 
      newListItem = newList[pos]
      oldListItem = oldList[pos]

      # When items from old and new don't match a mutation is required
      if newListItem != oldListItem
        posToSwap1 = pos
        posToSwap2 = oldList.indexOf newListItem

        # These numbers must swap places
        num1 = oldList[posToSwap1]
        num2 = oldList[posToSwap2]

        # Swap the numbers. Mutate the list
        oldList[posToSwap1] = num2
        oldList[posToSwap2] = num1

        # Make the promotion visible to the user
        @animateSuggestionPromotion(num2, num1)

        # Restart the process from the end of the lists
        setTimeout 1000, @determineListMutations(newList, oldList)


  animateSuggestionPromotion: (idToBeDemoted, idToBePromoted) ->
    console.log "idToBeDemoted = #{idToBeDemoted} || idToBePromoted = #{idToBePromoted} " 
    promoteSuggestion = $("[suggestion-id='#{idToBeDemoted}']")
    demoteSuggestion = $("[suggestion-id='#{idToBePromoted}']")

    demoteSuggestion.swap

    demoteSuggestion.animateCss "fadeOutRight"
    promoteSuggestion.animateCss "fadeOutLeft", ->
      demoteSuggestion.after(promoteSuggestion)

      demoteSuggestion.animateCss "fadeInLeft"
      promoteSuggestion.animateCss "fadeInRight"

    console.log promoteSuggestion
    console.log demoteSuggestion

    

  convertToArray: (objectListToConvert) ->
    list = []

    list.push sug.id for sug in objectListToConvert

    return list

  getOldSuggestionList: ->
    oldList = []
    
    # Old suggestion list is what is currently being displayed to the user
    $(@).find(".suggestion-row").each (i) ->
      oldList.push parseInt $(this).attr("suggestion-id")

    return oldList