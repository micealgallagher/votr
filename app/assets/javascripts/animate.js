$.fn.extend({
    animateCss: function (animationName, animationFinishedCallback) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
            if (animationFinishedCallback) {
              animationFinishedCallback();
            }
        });
    }
});