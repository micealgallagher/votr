# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
class @Suggestion
  suggestionWrapper: -> $("#suggestion_wrapper")
  suggestionInput: -> $("#suggestion_suggestion")
  timerFooter: -> $(".timeout-footer")
  votingFooter: -> $(".voting-footer")
  piechartTimer: -> $("#countdown")
  pinIcon: -> $("#pin-icon")
  pinInput: -> $("#pin-input")
  pinInputWrapper: -> $("#pin-input-wrapper")

  constructor: (@durationBetweenSuggestions, @pin, @campaignId, @clientId) ->
    @bindEvents()

  bindEvents: ->
    @clearSuggestionOnSubmitBinding()
    @nicknameAndPINInputBinding()
    @voteClickEventBinding()
    @preventDblClickOnSuggestionRows()

  nicknameAndPINInputBinding: ->

    localStoragePIN = localStorage.getItem "#{@campaignId}-PIN"
    localStorageNickname = localStorage.getItem "#{@campaignId}-nickname"
    localStorageClientId = localStorage.getItem "#{@campaignId}-clientId"
    localStorageCountdown = localStorage.getItem "countdown"

    allThreeProvided = localStoragePIN && localStorageNickname && localStorageClientId

    if allThreeProvided && localStoragePIN == "#{@pin}"
      console.log "MGDEV - RETURNER"
      @pinInputWrapper().remove()
      $("#enter-nickname-wrapper").remove()
      $("#fred").show()
      $("#suggestion_wrapper").show()
      $("#suggestion_wrapper").animateCss("zoomIn")

      if localStorageCountdown > 0
        @hideSuggestionSubmitForm(localStorageCountdown)

      @enableMixItUp()
      @disableSubmitSuggestionBinding()
    else
      @nicknameSubmittedBinding()
      @verifyCorrectPINBinding()

  nicknameSubmittedBinding: ->
    $("#submit-nickname").on "click", (e) =>

      nickname = $("#nickname").attr("placeholder")
      if $("#nickname").val()
        nickname = $("#nickname").val()

      localStorage.setItem "#{@campaignId}-nickname", nickname
      localStorage.setItem "#{@campaignId}-clientId", @clientId

      $("#enter-nickname").animateCss "fadeOutLeft", =>
        $("#enter-nickname").remove()

        $("#fred").show()
        $("#suggestion_wrapper").show()
        $("#suggestion_wrapper").animateCss "zoomIn"

        $("#voting-setup").remove()

        @enableMixItUp()

  verifyCorrectPINBinding: =>
    @pinInput().on 'keyup', (e) =>
      console.log  $(e.currentTarget).val()
      if $(e.currentTarget).val().length > 3
        if $(e.currentTarget).val() == "#{@pin}"

          localStorage.setItem "#{@campaignId}-PIN", "#{@pin}"

          @pinInput().attr("disabled" , "true")
          @pinIcon().removeClass "glyphicon glyphicon-lock"   
          @pinIcon().addClass "pin-green fa fa-unlock"

          @pinIcon().animateCss "hinge"

          @pinInputWrapper().animateCss "fadeOutLeft", =>
            @pinInputWrapper().hide()

            $("#enter-nickname").show()
            $("#enter-nickname").animateCss("fadeInRight")
        else
          @pinIcon().removeClass("pin-amber")
          @pinIcon().addClass("pin-red")
          @pinIcon().animateCss "shake", =>
            @pinIcon().removeClass("pin-red")
            @pinIcon().addClass("pin-amber")


  clearSuggestionOnSubmitBinding: ->
    $("#new_suggestion").bind "ajax:success", (e, data, status, xhr) =>
      @suggestionInput().val("")
      @hideSuggestionSubmitForm(@durationBetweenSuggestions)

  disableSubmitSuggestionBinding: ->
    @disableSubmitSuggestion()
    
    @suggestionInput().on 'keyup', (e) =>
      @disableSubmitSuggestion($(e.currentTarget))

  disableSubmitSuggestion: ->
    submitButton = $("#suggestion-submit")
    
    # Adding suggestions maybe disabled
    if @suggestionInput().length > 0
      # Enable submit button when suggestion has been provided
      if @suggestionInput().val().length == 0
        submitButton.prop('disabled', true)
        submitButton.addClass('suggestion-submit-button-disabled')
      else
        submitButton.prop('disabled', false)
        submitButton.removeClass('suggestion-submit-button-disabled')

  voteClickEventBinding: ->
    @suggestionWrapper().on 'click', '.suggestion-row', (e) =>
      suggestionRow = $(e.currentTarget)

      if suggestionRow.hasClass "suggestion-row-voted"
        suggestionId = suggestionRow.attr('suggestion-id')
        suggestionRow.removeClass("suggestion-row-voted")

        # We no longer need to highligh this suggestion
        App.voting.getArrayOfVotedSuggestionIds().splice( $.inArray(suggestionId, App.voting.voted), 1 );
        App.voting.unvote(suggestionId)
      else
        suggestionId = suggestionRow.attr('suggestion-id')
        suggestionRow.addClass("suggestion-row-voted")

        # Keep track of what was upvoted so we can rehighlight when list is refreshed
        App.voting.getArrayOfVotedSuggestionIds().push parseInt suggestionId
        App.voting.upvote(suggestionId)

  preventDblClickOnSuggestionRows: ->
    @suggestionWrapper().children().on 'dblclick', '.suggestion-row', (e) =>
      e.preventDefault

  enableMixItUp: ->
    selectors = {}
    options = {}
    layout = {}

    selectors["target"] = ".suggestion-row"
    layout["display"] = "inherit"
    
    options["selectors"] = selectors
    options["layout"] = layout

    @suggestionWrapper().mixItUp options

  hideSuggestionSubmitForm: (countdownDuration)->
    # Prepare the countdown360 optionsz
    options = {}
    options["radius"] = 30
    options["seconds"] = countdownDuration
    options["label"] = false
    options["fontColor"] = '#FFFFFF'
    options["autostart"] = false
    options["fillStyle"] = '#D4ADCF'
    options["strokeStyle"] = '#856084'
    
    options["onComplete"] = =>
      # Remove the timer footer and display the suggestion submission form
      @timerFooter().animateCss 'zoomOutRight', =>
        @timerFooter().hide()
        @votingFooter().show()
        @votingFooter().animateCss 'zoomInLeft'

    @piechartTimer().countdown360(options)

    # Remove the suggestion submission form and show the countdown timer
    @votingFooter().animateCss 'zoomOutRight', =>
      @votingFooter().hide()
      @timerFooter().show()
      @piechartTimer().countdown360(options).start()