App.moderate = null

$(document).on 'turbolinks:load', ->

  if $("#moderate-page").length > 0
    campaignId = $("#moderate-page").val();

    if App.moderate == null

      App.moderate = App.cable.subscriptions.create {channel: "VotingChannel", campaign_id: $("#moderate-page").val() },
          
          connected: ->
            console.log "MGDEV - Moderate - connected"
            @subscribeToCampagin()
            # Called when the subscription is ready for use on the server
            setTimeout =>
              @installPageChangeCallback()
            , 1000
          
          disconnected: ->
            # Called when the subscription has been terminated by the server
            console.log "MGDEV - Moderate - DISCONNECTED"
          
          received: (data) ->
            # Called when there's incoming data on the websocket for this channel
            console.log "MGDEV - Moderate - Recieved:" 
            console.log data

            if ( data.command == "NewSuggestion" )
              @actionNewSuggestion data
            else-if ( data.command == "NewCurrentSuggestion" )
              @actionNewCurrentSuggestion data
            else-if ( data.command == "CloseCampaign" )
              @actionCloseCampaign data

          subscribeToCampagin: ->
            if $("#moderate-page").length > 0
              console.log "MGDEV - Moderate - unsubscribe-subscribe"
              @perform 'unsubscribe'
              @perform 'subscribe'
            else
              console.log "MGDEV - Moderate - unsubscribe"
              @perform 'unsubscribe'

          unsubscribeFromCampaign: ->
            console.log "MGDEV - Moderate - unsubscribe"
            @perform 'unsubscribe'

          installPageChangeCallback: ->
            unless @installedPageChangeCallback
              @installedPageChangeCallback = true
              $(document).on 'turbolinks:load', -> App.moderate.subscribeToCampagin()

          actionNewSuggestion: (data) ->
            console.log  "appending"
            $("#moderate-pending-suggestions").append(data.message)
            @incrementPendingCount()

          actionNewSuggestion: (data) ->
            $("#moderate-pending-suggestions").append(data.message)
            @incrementPendingCount()

          actionCloseCampaign: (data) ->
            console.log "MGDEV - We care calling"
            console.log $("#overview-suggestion")[0]
            
            $("#overview-suggestion").animateCss "fadeOut", ->
              $("#overview-suggestion").children().remove()
              $("#overview-suggestion").append(data.message)
              $("#overview-suggestion").animateCss "fadeIn"

          actionNewCurrentSuggestion: (data) ->
            $("#countdown").removeClass("infinite")
            $("#next-suggestion").animateCss "zoomOut"
            $("#current-suggestion").animateCss "zoomOut", ->
#              $("#current-suggestion").css "visibility", "hidden"

              most_voted_suggestion = data.message["most_voted_suggestion"]
              most_voted_suggestion ?= "N/a"

              $("#current-suggestion-text").text(data.message["current_suggestion"])
              $("#next-suggestion-text").text(most_voted_suggestion)

              $("#next-suggestion").animateCss "zoomIn"
              $("#current-suggestion").animateCss "zoomIn"

              countdownDuration = parseInt(data.message["remaining_duration"])
              $("#countdown").attr("data-countdown", countdownDuration)

              $("#countdown").countdown360().restart(countdownDuration)
            

          incrementPendingCount: ->
            pendingCountBadge = $("#pending-count");
            count = parseInt pendingCountBadge.attr("data-value")
            count = count + 1

            pendingCountBadge.addClass('animated fadeIn')
            pendingCountBadge.css("display", "")
            pendingCountBadge.text(count)
            pendingCountBadge.attr("data-value", count)