class @Overview
  countdown: -> $("#countdown")

  constructor: ->
    @enableCountdown()

  enableCountdown: ->
    countdownDuration = @countdown().attr("data-countdown")

    options = {}
    options["radius"] = 30
    options["seconds"] = countdownDuration
    options["label"] = false
    options["fontColor"] = '#FFFFFF'
    options["autostart"] = true
    options["fillStyle"] = '#D4ADCF'
    options["strokeStyle"] = '#856084'
    options["startOverAfterAdding"] = true
    
    options["onComplete"] = =>
      console.log "Coplete"
      @countdown().animateCss 'pulse infinite'

    console.log "MGDEV - beginning counter"
    @countdown().countdown360(options)