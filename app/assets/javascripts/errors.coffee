class @Error
  constructor: (name, message) ->
    $ ->
      control = $("[id*='#{name}']")
      label_wrapper = $("label[for='#{name}']").parent()

      console.log "[id*='_#{name}']"
      console.log control[0]
      control.addClass("control-with-errors")

      message_div = $("<div/>",{
          text: message
          class: 'field-with-errors pull-right animated pulse infinite'
          style: '-webkit-font-smoothing: subpixel-antialiased'
      }) 
      label_wrapper.append(message_div)