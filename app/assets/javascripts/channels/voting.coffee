App.voting = null

ready = ->
  console.log "MGDEV - attempting to load voting.coffee"
  if $("#voting-page").length > 0
    campaignId = $("#voting-page").val();

    if App.voting == null

      App.voting = App.cable.subscriptions.create {channel: "VotingChannel", campaign_id: campaignId },
        voted: []
        
        connected: ->
          console.log "MGDEV - Voting - connected"
          @subscribeToCampagin()
          # Called when the subscription is ready for use on the server
          setTimeout =>
            @installPageChangeCallback()
          , 1000
        
        disconnected: ->
          # Called when the subscription has been terminated by the server
          console.log "MGDEV - Voting - DISCONNECTED"
        
        received: (data) ->
          # Called when there's incoming data on the websocket for this channel
          console.log data
          if ( data.command == "ApprovedSuggestion" )
            @actionApprovedSuggestion data
          else-if ( data.command == "RejectedSuggestion" )
            @actionRejectSuggestion data
          else-if ( data.command == "VotingChanged" )
            @actionVotingChanged data
          else-if ( data.command == "NewCurrentSuggestion" )
            @actionNewCurrentSuggestion data

        upvote: (suggestionId) ->
          @perform 'upvote', suggestion_id: suggestionId
        
        unvote: (suggestionId) ->
          @perform 'unvote', suggestion_id: suggestionId
        
        subscribeToCampagin: ->
          if $("#voting-page").length > 0
            console.log "MGDEV - Voting - unsubscribe-subscribe"
            @perform 'unsubscribe'
            @perform 'subscribe'
          else
            console.log "MGDEV - Voting - unsubscribe"
            @perform 'unsubscribe'

        unsubscribeToCampagin: ->
          console.log "MGDEV - Voting - unsubscribe"
          @perform 'unsubscribe'
        
        installPageChangeCallback: ->
          unless @installedPageChangeCallback
            @installedPageChangeCallback = true
            $(document).on 'turbolinks:load', -> App.voting.subscribeToCampagin()
        
        actionApprovedSuggestion: (data) ->
          suggestion = data.message
          item = $("#suggestion_wrapper").append(data.message)

          console.log "Up next"
          console.log App.voting.voted
          console.log data.id

          if App.voting.voted.indexOf(data.id) > -1
            console.log $("[suggestion-id='#{data.id}']")
            $("[suggestion-id='#{data.id}']").addClass("suggestion-row-voted")

          @updateRanksAndSort()

        actionRejectSuggestion: (data) ->
          suggestionId = data.message
          suggestion = $("[suggestion-id='#{suggestionId}'")

          # Remove the suggestion and re-sort the suggestions
          suggestion.animateCss "fadeOutLeft", =>
            suggestion.remove()
            App.voting.updateRanksAndSort()

        actionVotingChanged: (data) ->
          suggestions = data.message

          # Update votes for each suggestion
          for suggestion, i in suggestions
            element = $("[suggestion-id='#{suggestion.id}'")
            
            element.attr("data-votes", suggestion.votes)
            element.attr("data-updated", suggestion.updated_at_as_integer)

            $("#suggestion_#{suggestion.id}_votes").text(suggestion.votes)
            $("#suggestion_#{suggestion.id}_rank").text(i + 1)
    
          $("#suggestion_wrapper").mixItUp('sort', 'votes:desc, updated:asc');

        actionNewCurrentSuggestion: (data) ->
          currentSuggestionId = data.message["current_suggestion_id"]
          suggestion = $("[suggestion-id='#{currentSuggestionId}'")
          suggestion.remove()
          @updateRanksAndSort()

        updateRanksAndSort: ->
          console.log @getArrayOfVotedSuggestionIds()
          # Sort
          $("#suggestion_wrapper").mixItUp 'sort', 'votes:desc', ->

            # Re-rank
            suggestionVoteElements = $("#suggestion_wrapper .rank")
            for element, i in suggestionVoteElements
              element = $(element)
              
              curentRank = parseInt(element.text())
              newRank = i+1

              # Only update the rank when it differs and animate the actionVotingChanged
              if curentRank != newRank
                element.text(newRank)
                element.animateCss("fadeInUp")

        getArrayOfVotedSuggestionIds: ->
          voted = localStorage.getItem "#{campaignId}-voted"
          if !voted
            voted = []

          return voted



$(document).on 'turbolinks:load', ready