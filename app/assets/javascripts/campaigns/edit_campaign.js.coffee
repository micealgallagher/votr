class @EditCampaign
	constructor: ->
		$ ->
			$('[data-toggle="tooltip"]').tooltip({trigger: 'click'})

		@enableBootstrapSwitch()
		@showHideSuggestionOptions $("#campaign_allow_suggestions").prop "checked"
		@showHideCurrentSuggestionDuration $("#campaign_manual_advance").prop "checked"
		@bindEvents()

	# enableManualSuggestionApproval: ->
	# 	if ()
	# 		$("#campaign_approve_suggestions").prop "disabled", false
	# 		$("#campaign_duration_between_suggestions").prop "disabled", false
	# 	else
	# 		$("#campaign_approve_suggestions").prop "checked", false
	# 		$("#campaign_approve_suggestions").prop "disabled", true
	# 		$("#campaign_duration_between_suggestions").prop "disabled", true

	enableBootstrapSwitch: ->
		options = {}
		options["size"] = "small"
		options["onText"] = "Allow"
		options["offText"] = "Block"
		options["onSwitchChange"] = (event, state) =>
			@showHideSuggestionOptions state
			

		$("#campaign_allow_suggestions").bootstrapSwitch(options)

		options = {}
		options["size"] = "small"
		options["onText"] = "Yes"
		options["offText"] = "No"
		
		$("#campaign_approve_suggestions").bootstrapSwitch(options)

		options = {}
		options["size"] = "small"
		options["onText"] = "Manually"
		options["offText"] = "Automatically"
		options["onSwitchChange"] = (event, state) =>
			@showHideCurrentSuggestionDuration state
		
		$("#campaign_manual_advance").bootstrapSwitch(options)

	showHideSuggestionOptions: (state) ->
		if state
			$("#suggestion-options").show("slow")
		else
			$("#suggestion-options").hide("slow")

	showHideCurrentSuggestionDuration: (state) ->
		if state
			$("#current-suggestion-duration-wrapper").hide("slow")
		else
			$("#current-suggestion-duration-wrapper").show("slow")

	bindEvents: ->
		@submitCampaignEvent()

	submitCampaignEvent: ->
		$("#campaign_form").bind "ajax:success", (e, data, status, xhr) =>
			$(".field-with-errors").remove()
			$(".control-with-errors").removeClass("control-with-errors")
			if data["error"]
				$(".modal-content").animateCss "shake"
				$("#campaign_form_wrapper").animateCss "shake"
				for name, message of data["error"]
					new Error("campaign_#{name}", message)