# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class VotingChannel < ApplicationCable::Channel

  def subscribe(data)
    # stream_from "some_channel"
    logger.debug "MGDEV #{params}"
    stop_all_streams
    stream_from "campaign:#{params['campaign_id']}:voting"
  end

  def speak(data)
    ActionCable.server.broadcast('suggestion',
      suggestion: render_suggestion)
  end

  def receive(data)
    VotingChannel.broadcast_to("suggestion", data)
  end

  def unsubscribe
    # Any cleanup needed when channel is unsubscribed
    stop_all_streams
  end

  def upvote(data)
    logger.debug "MGDEV - campaign id #{data['suggestion_id']}"
    SuggestionController.upvote(data['suggestion_id'])
  end

  def unvote(data)
    SuggestionController.unvote(data['suggestion_id'])
  end

  private

  def render_suggestion(suggestion)
    ApplicationController.render(partial: 'suggestion/suggestion',
                                 locals: {suggestion: suggestion})
  end


end
