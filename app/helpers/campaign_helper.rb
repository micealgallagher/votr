module CampaignHelper
	def self.get_audio_cues
    ['None', 'Buzz', 'Dong', 'Ding']
  end

  def self.get_duration_options_between_suggestions
    [10, 20, 30, 50, 80, 130, 210]
  end

  def self.get_current_suggesiton_ttl_options
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  end

  def self.explain_closed_reason(reason)
    message = "Unknown Reason! #{reason}"
    case reason
      when :NO_REMAINING_SUGGESTIONS.to_s
        message = "Closed automatically as there were no suggestions remaining"
    end

    return message
  end

end
