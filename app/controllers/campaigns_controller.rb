class CampaignsController < ApplicationController
	before_action :get_campaign, only: [:edit, :show, :update, :activate, :activation, :deactivate]

	def new
		@campaign = Campaign.new
	end

	def create
		@campaign = Campaign.new(campaign_params)
		
		if @campaign.save()

			if @campaign.initial_suggestions
				initial_suggestions = @campaign.initial_suggestions.each_line 

				initial_suggestions.map {|suggestion| 
					@campaign.suggestions.new(suggestion: suggestion.strip, initial: true, approval_status: :APPROVED).save 
				}
			end
			
      redirect_to @campaign

		else
	    respond_to do |format|
	      format.json { render :json => { :error => @campaign.errors } }
	    end
		end
	end

	def edit
	end

	def show		
		suggestions = @campaign.suggestions.initial.pluck(:suggestion)
		
		@campaign.initial_suggestions = ''
		suggestions.each do |s|
			@campaign.initial_suggestions += s.strip + "\r"
			@campaign.initial_suggestions.strip
		end
	end

	def update
		if @campaign.update_attributes(campaign_params)

			@campaign.suggestions.initial.delete_all

			if @campaign.initial_suggestions
				initial_suggestions = @campaign.initial_suggestions.each_line

				initial_suggestions.map {|suggestion| 
					@campaign.suggestions.new(suggestion: suggestion.strip, initial: true).save 
				}
			end
			
			redirect_to @campaign
		else
			respond_to do |format|
	      format.json { render :json => { :error => @campaign.errors } }
	    end
		end

	end

	def index
		@campaigns = Campaign.all();
		@campaign = Campaign.new
	end

	def activate
		# Make the most voted suggestion the current suggestion
		most_voted_suggestion = @campaign.suggestions.most_voted
		if most_voted_suggestion.any?
			most_voted_suggestion = most_voted_suggestion.first
			most_voted_suggestion.make_current
			if most_voted_suggestion.save
				logger.debug "most_voted_suggestion saved successfully"
			else
				logger.debug "to shreds you say?"
			end
			@campaign.activate

			if @campaign.save(:validate => false)
				logger.debug "campaign saved successfully"
			else
				logger.debug "campaign to shreds you say?"
			end

			AdvanceSuggestionJob.set(wait: @campaign.current_suggestion_ttl_as_seconds).perform_later(@campaign.id)

			flash[:title] = "Campaign Activated"
			flash[:notice] = "Campaign has been actived. Current suggestion is \"#{most_voted_suggestion.suggestion}\""
		else 
			flash[:title] = "Cannot Activate Campaign"
			flash[:notice] = "Campaigns can only be actived if they have suggestions"
		end

		redirect_to activation_campaign_path(@campaign)
	end

	def deactivate
		@campaign.close :MANUALLY_CLOSED
		@campaign.save

		flash[:title] = "Campaign Deactivated"
		flash[:notice] = "Campaign has been deactivated and closed"

		redirect_to activation_campaign_path(@campaign)
	end


private
	def campaign_params
		params.require(:campaign).permit(:id, :name, :audio_cue, :start, :stop, :initial_suggestions, :allow_suggestions, :approve_suggestions, :duration_between_suggestions, :current_suggestion_ttl)
  end

  def get_campaign
  	@campaign = Campaign.find(params[:id]) if params[:id].present?
  end

end