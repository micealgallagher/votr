class VotingController < ApplicationController
	layout "voting"
	
  def index
    @campaign = Campaign.find(params[:campaign_id])
    @suggestion = Suggestion.new(campaign: @campaign)
    @suggestions = @campaign.suggestions.approved_not_archived
	end

  private
  
  def voting_params
    params.require(:suggestion).permit(:campaign_id, :suggestion)
  end
end