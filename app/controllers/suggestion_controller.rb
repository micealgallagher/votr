require 'digest'

class SuggestionController < ApplicationController
  layout "voting"

  before_action :get_campaign

  def create
    @suggestion = Suggestion.new(suggestion_params)
    @suggestion.campaign = @campaign

    rank = Suggestion.where(campaign_id: @campaign.id).count

    @suggestion.save()

    if @campaign.approve_suggestions
      ModerateController.test(@campaign, @suggestion)
    else
      ApprovedSuggestionJob.perform_later(campaign_id, rank, @suggestion.id)
    end

    respond_to do |format|
      format.js { head :ok } 
    end

  end

  def self.upvote(suggestion_id)
    suggestion = Suggestion.find(suggestion_id)
    suggestion.update_attribute(:votes, suggestion.votes + 1)
    
    VotingChangedJob.perform_later(@campaign.id)
  end


  def self.unvote(suggestion_id)
    suggestion = @campaing.suggestions.find(suggestion_id)
    suggestion.update_attribute(:votes, suggestion.votes - 1)

    VotingChangedJob.perform_later(@campaign.id)
  end

  def new
    @suggestion = Suggestion.new(campaign_id: @campaign)
    @suggestions = @campaign.suggestions.approved_not_archived
    @random_nickname = Nickname.random()
    @client_id = Digest::SHA1.hexdigest("some-random-string")[8..16]
  end

  def approve
    suggestion = @campaign.suggestions.find(approve_params[:suggestion_id])

    suggestion.approve
    suggestion.save()

    @selected_tab = params[:tab]

    rank = @campaign.suggestions.approved_not_archived.count

    ApprovedSuggestionJob.perform_later(@campaign.id, rank, suggestion.id)

    redirect_to campaign_moderate_path(@campaign, {:tab => @selected_tab})
  end

  def reject
    suggestion = Suggestion.find(params[:suggestion_id])

    suggestion.reject
    suggestion.save()

    @selected_tab = params[:tab]


    RejectedSuggestionJob.perform_later(@campaign.id, suggestion.id)

    redirect_to campaign_moderate_path(@campaign, {:tab => @selected_tab})
  end

  private

  def suggestion_params
    params.require(:suggestion).permit(:suggestion, :suggestion_id, :tab, :campaign_id)
  end

  def approve_params
    params.permit(:suggestion_id, :tab, :campaign_id)
  end

  def campaign_suggestion_params
      params.permit(:campaign_id)
  end

  def get_campaign
    @campaign = Campaign.find(campaign_suggestion_params[:campaign_id])
  end

end
