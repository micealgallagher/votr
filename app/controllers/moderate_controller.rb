class ModerateController < ApplicationController
  before_action :get_campaign, only: [:show]

  def show
    @suggestions_pending_approval = @campaign.suggestions.pending_not_archived
    @suggestions_approved = @campaign.suggestions.approved_not_archived
    @suggestions_rejected = @campaign.suggestions.rejected_not_archived

    if @campaign.active 
      current     = @campaign.suggestions.current
      most_voted  = @campaign.suggestions.most_voted

      @current_suggestion = "N/a"
      @most_voted_suggestion = "N/a"      
      
      if current.any?
        current = current.first
        @current_suggestion = current.suggestion
        @remaining_duration_as_current = @campaign.current_suggestion_ttl_as_seconds - (Time.now.to_i - current.made_current_at.to_i)

        @remaining_duration_as_current = 0 if @remaining_duration_as_current < 0
      end

      if most_voted.any?
        most_voted = most_voted.first
        @most_voted_suggestion = most_voted.suggestion
      end

    end

    @selected_tab = moderate_params[:tab]
  end

  def self.test(campaign, suggestion)

    logger.debug "MGDEV - campaign_id: #{campaign.id} + suggestion_id: #{suggestion.id}"

    ActionCable.server.broadcast "campaign:#{suggestion.campaign_id}:voting", {
      command: 'NewSuggestion',
      message: ModerateController.render(
        partial: 'moderate/single_row',
        locals: {campaign_id: campaign.id, campaign: campaign, suggestion: suggestion, show_approve_action: true, show_reject_action: true, tab: 'pending'}
      ).squish
    }
  end

  def moderate_params
    params.permit(:tab)
  end

  def get_campaign
    @campaign = Campaign.find(params[:campaign_id])
  end
end
