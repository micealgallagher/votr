class Nickname < ApplicationRecord
  scope :random, -> { order("RANDOM()").first.name }
end
