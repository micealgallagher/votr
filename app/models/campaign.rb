class Campaign < ApplicationRecord
	has_many :suggestions

  before_save :set_duration_between_suggestions
  before_create :generate_pin

  validates :name, presence: true
  validates :initial_suggestions, presence: true
	
	attr_accessor :initial_suggestions

  def current_suggestion_ttl_as_seconds
    (self.current_suggestion_ttl * 10).seconds
  end

  def set_duration_between_suggestions
    self.duration_between_suggestions = -1 unless self.allow_suggestions
  end

  def generate_pin
    self.pin = Random.rand(1000...9999)
  end

  def activate
    self.active = true
    self.was_active = true
  end

  def close( close_reason )
    self.active = false
    self.closed_reason = close_reason
  end

  def was_active_is_inactive
    was_active && !active
  end

  def self.reset_all
    Campaign.all.each do |c|
      c.update_columns(:was_active => false, :active => false, :closed_reason => nil)
    end
    Suggestion.update(:archived => false, :current => false)
  end

end