class Suggestion < ApplicationRecord
	belongs_to :campaign

  before_create :set_creation_approval

  scope :initial,       -> { where( initial: true ) }
  scope :approved,      -> { where( approval_status: 'APPROVED' ) }
  scope :pending,       -> { where( approval_status: 'PENDING' ) }
  scope :rejected,      -> { where( approval_status: 'REJECTED') }
  scope :not_archived,  -> { where( archived: false ) }
  scope :archived,      -> { where( archived: true ) }
  scope :sort,          -> { order( votes: :desc, updated_at: :asc ) }
  scope :not_current,   -> { where( current: false ) }
  scope :current,       -> { where( current: true ) }

  default_scope { order(votes: :desc, updated_at: :asc) } 

  class << self
    def approved_not_archived
      approved.not_archived.not_current
    end

    def rejected_not_archived
      rejected.not_archived.not_current
    end

    def pending_not_archived
      pending.not_archived.not_current
    end

    def most_voted
      not_archived.not_current.limit(1) 
    end
  end

  def set_creation_approval
    # Campaign creators make initial suggestions, approve by default
    self.approval_status = "APPROVED" if initial
    self.approval_status ||= campaign.approve_suggestions ? "PENDING" : "APPROVED"
  end

  def updated_at_as_integer
    updated_at.to_i
  end

  def make_current
    self.current = true
    self.made_current_at = Time.now
  end

  def approve
    self.approval_status = "APPROVED"
  end

  def reject
    self.approval_status = "REJECTED"
  end

  def pending
    self.approval_status = "PENDING"
  end

  def archive
    self.archived = true
    self.current = false
    self.made_current_at = nil
  end


end
